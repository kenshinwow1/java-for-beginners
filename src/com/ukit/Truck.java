package com.ukit;

public class Truck extends Transport {

    private boolean isloaded;

    public Truck (int weight, byte[] coordinate){
       super(weight, coordinate);


    }

    public Truck (int weight, byte[] coordinate, boolean isloaded){
        super(weight, coordinate);
        this.isloaded = isloaded;


    }

    public void setIsloaded(boolean loaded) {
        isloaded = loaded;
    }

    public void getloaded(){
        if (isloaded == true)
            System.out.println("Gruzovik zagrujen");
        else
            System.out.println("Gruzovik svoboden");
    }
}
